#include "core/pod/entity-manager.h"
#include "core/pod/component-pool.h"

#include <cstdio>

void pause()
{
	system("pause");
}

struct Foo {
	int a;
	int b;
};

int main(int argc, char* argv[])
{
	atexit(pause);
	EntityManager ep;
	Entity ent = ep.create();
	
	if (ep.has(ent)) {
		puts("itz haz");
	}

	ComponentPool cp(ep, sizeof(Foo));

	Foo* f;
	f = cp.get_as<Foo>(ent);
	cp.allocate(ent);
	f = cp.get_as<Foo>(ent);
	return 0;
}