#ifndef POOL_H
#define POOL_H

#include <cstdlib>
#include <cstdint>
#include <memory>

#include "handle.h"

typedef Handle Entity;

class EntityManager {
public:
	EntityManager();
	~EntityManager();

	// Creates an entity handle
	Entity create();

	// Checks if an entity exists in the entity pool.
	bool has(Entity entity) const;

private:
	EntityManager(const EntityManager&);
	EntityManager& operator=(const EntityManager&);

	Entity* m_entities;
	size_t m_count;
	size_t m_capacity;
};
#endif // pool.h 