#ifndef HANDLE_H
#define HANDLE_H

#include <cstdint>

#define INVALID_HANDLE 0xFFFFFFFF

struct Handle {
	uint32_t aux : 1;	// multipurpose bit
	uint32_t count : 7;
	uint32_t index : 24;
};

#endif // handle.h