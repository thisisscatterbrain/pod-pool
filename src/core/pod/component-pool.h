#ifndef COMPONENT_POOL_H
#define COMPONENT_POOL_H

#include "entity-manager.h"

// Manages memory for component managers. Assures all objects are stored in
// contigous memory; Reallocates components, so reference them by handle not
// by pointer!
class ComponentPool {
public:
	ComponentPool(const EntityManager& entity_manager, 
		size_t element_size);
	virtual ~ComponentPool();

	// Allocates an object that can be indexed by "entity"'s index.
	void allocate(Entity entity);
	bool has(Entity entity);
	void* get(Entity entity);
	const void* get(Entity entity) const;

	template <class T>
	T* get_as(Entity entity);

	template <class T>
	const T* get_as(Entity entity) const;
	
	// TODO 
	void deallocate(Entity entity);

private:

	// Needed for checking if an entity exists.
	const EntityManager* m_entity_mgr;

	// Used for mapping an entity to an object
	Entity* m_entity_map;
	size_t m_entity_capacity;

	void* m_buffer;
	size_t m_element_size;
	size_t m_count;
	size_t m_capacity;
};


template <class T>
T* ComponentPool::get_as(Entity entity)
{
	return static_cast<T*>(get(entity));
}

template <class T>
const T* ComponentPool::get_as(Entity entity) const
{
	return get<T>(entity);
}


#endif // component-pool.h