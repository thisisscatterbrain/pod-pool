#include "entity-manager.h"
#include "../assertions.h"
#include "../alloc.h"
#include <memory>

#define INITITIAL_CAPACITY	8
#define GROW			2

EntityManager::EntityManager() : m_count(0), m_capacity(INITITIAL_CAPACITY)
{
	m_entities = gen_allocate<Handle>(m_capacity);
	ASSERT(m_entities)
	memset(m_entities, 0, sizeof(*m_entities) * m_capacity);
}

EntityManager::~EntityManager()
{
	delete m_entities;
}

// Searches for the first index for which "handles" does not contain a currently
// used handle. If no such index could be found "count" is returned.
static size_t __PRIVATE__get_free_handle(const Handle* handles, 
	size_t count)
{
	for (size_t i = 0; i < count; i++) {
		if (!handles[i].aux) {
			// the aux bit is used to indicate if a handle is
			// used for the entity manager.
			return i;
		}
	}

	return count;
}

Entity EntityManager::create()
{
	size_t id = __PRIVATE__get_free_handle(m_entities, m_count);

	if ((id == m_count) && (m_count == m_capacity)) {
		m_entities = gen_recallocate<Entity>(m_entities, m_capacity, 
			GROW * m_capacity);
		ASSERT(m_entities);
		m_capacity = GROW * m_capacity;
	}

	m_entities[id].aux = 1;
	m_entities[id].count++;
	m_entities[id].index = id;
	m_count++;
	return m_entities[id];
}

bool EntityManager::has(Entity entity) const
{
	ASSERT(entity.index < m_count);
	return (entity.count == m_entities[entity.index].count) && (m_entities[entity.index].aux);
}