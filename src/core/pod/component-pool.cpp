#include "component-pool.h"
#include "../alloc.h"
#include "../assertions.h"

#define INITITIAL_CAPACITY	8
#define GROW			2

ComponentPool::ComponentPool(const EntityManager& entity_manager, size_t element_size) :
	m_entity_mgr(&entity_manager),
	m_entity_capacity(INITITIAL_CAPACITY), m_element_size(element_size),
	m_count(0), m_capacity(INITITIAL_CAPACITY)
{
	m_entity_map	= gen_allocate<Handle>(m_entity_capacity);
	m_buffer	= calloc(m_capacity, sizeof(m_element_size));
}

ComponentPool::~ComponentPool() 
{
	free(m_entity_map);
	free(m_buffer);
}


void ComponentPool::allocate(Entity entity)
{
	ASSERT(m_entity_mgr->has(entity))

	if (entity.index >= m_entity_capacity) {
		size_t newcap = m_entity_capacity;

		while (newcap < entity.index) {
			newcap *= GROW;
		}

		gen_recallocate<Entity>(m_entity_map, m_entity_capacity, newcap);
		m_entity_capacity = newcap;
	}

	uint32_t id = entity.index;
	ASSERT(entity.aux)
	m_entity_map[id].aux = entity.aux;
	m_entity_map[id].count = entity.count;

	// allocate the object at the end of the array.
	m_entity_map[id].index = m_count;

	if (m_count == m_capacity) {
		m_buffer = recallocate(m_buffer, m_element_size, m_capacity, 
			GROW * m_capacity);
		m_capacity = GROW * m_capacity;
	}

	m_count++;
}

bool ComponentPool::has(Entity entity)
{
	ASSERT(m_entity_capacity > entity.index)
	Handle handle = m_entity_map[entity.index];
	return handle.aux && (handle.count == entity.count);
}

void* ComponentPool::get(Entity entity)
{
	if (!has(entity)) {
		return nullptr;
	}

	return static_cast<uint8_t*>(m_buffer) + 
		m_element_size * m_entity_map[entity.index].index;
}

const void* ComponentPool::get(Entity entity) const 
{
	return get(entity);
}
