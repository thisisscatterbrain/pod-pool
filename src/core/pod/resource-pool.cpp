#include "resource-pool.h"
#include "../assertions.h"
#include "../alloc.h"
#include <memory>

#define INITITIAL_CAPACITY	8
#define GROW			2

ResourcePool::ResourcePool(size_t element_size) : 
	m_handle_capacity(INITITIAL_CAPACITY), m_element_size(element_size),
	m_count(0), m_capacity(INITITIAL_CAPACITY)
{
	m_handle_map = gen_allocate<Handle>(m_handle_capacity);
	ASSERT(m_handle_map);
	m_buffer = calloc(m_capacity, element_size);
	ASSERT(m_buffer);
}

ResourcePool::~ResourcePool()
{
	free(m_handle_map);
	free(m_buffer);
}