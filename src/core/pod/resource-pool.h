#ifndef RESOURCE_POOL_H
#define RESOURCE_POOL_H

#include "handle.h"

class ResourcePool {
public:
	ResourcePool(size_t element_size);
	virtual ~ResourcePool();

	Handle allocate();
	bool has(Handle handle);
	void* get(Handle handle);
	const void* get(Handle handle) const;

private:
	// Used for mapping an entity to an object
	Handle* m_handle_map;
	size_t m_handle_capacity;

	void* m_buffer;
	size_t m_element_size;
	size_t m_count;
	size_t m_capacity;
};

#endif // resource-pool.h