#ifndef ASSERTIONS_H
#define ASSERTIONS_H

#ifdef _MSC_VER
	#define DEBUG_BREAK __debugbreak();
#else
	#define DEBUG_BREAK
#endif

void report(const char* filename, int line, const char* condition);
void reportf(const char* filename, int line, const char* condition,
	const char* format, ...);

#define ASSERT( CONDITION )							\
	if (!(CONDITION)) {							\
		report(__FILE__, __LINE__, #CONDITION);				\
		DEBUG_BREAK							\
	}

#define ASSERTF( CONDITION, FORMAT, ...)					\
	if (!(CONDITION)) {							\
		reportf(__FILE__, __LINE__, #CONDITION, (FORMAT), __VA_ARGS__);	\
		DEBUG_BREAK							\
	}

#define FAIL(FORMAT, ...) {							\
		reportf(__FILE__, __LINE__, nullptr, (FORMAT), __VA_ARGS__);	\
		DEBUG_BREAK }	

#define WARN(CONDITION, FORMAT, ...)							\
		if (!(CONDITION)) {							\
			reportf(__FILE__, __LINE__, nullptr, (FORMAT), __VA_ARGS__);	\
		}



#endif // assertions.h