#include "alloc.h"

void* recallocate(void* buffer, size_t element_size, size_t old_count,
	size_t new_count)
{
	void* newbuf = calloc(element_size, new_count);

	if (!newbuf) {
		return nullptr;
	}

	memcpy(newbuf, buffer, element_size * old_count);
	free(buffer);
	return newbuf;
}