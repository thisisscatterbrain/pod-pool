#include "assertions.h"
#include <cstdio>
#include <cstdarg>

void report(const char* filename, int line, const char* condition)
{
	printf("%s (%d) : \n\t Assertion \"%s\" failed \n", filename, 
		line, condition);
}

void reportf(const char* filename, int line, const char* condition, 
	const char* format, ...)
{
	printf("%s (%d) :\n", filename, line);

	if (condition)
		printf("\t Assertion \"%s\" failed \n", condition);

	va_list args;
	va_start(args, format);
	vfprintf(stderr, format, args);
	va_end(args);
}