#ifndef ALLOC_H
#define ALLOC_H

#include <cstdlib>
#include <memory>

template<typename T>
T* gen_allocate(size_t count)
{
	return static_cast<T*>(calloc(count, sizeof(T)));
}


void* recallocate(void* buffer, size_t element_size, size_t old_count, 
	size_t new_count);

template<typename T>
T* gen_recallocate(T* buffer, size_t count, size_t new_count)
{
	T* newbuf = gen_allocate<T>(new_count);
	memcpy(newbuf, buffer, count * sizeof(T));
	free(buffer);
	return newbuf;
}

#endif // alloc.h